let totalBalance = 0
let moneyWorkedFor = 0
let loanedMoney = 0
let komputers = []
let selectedLaptop

const toBePayed = document.getElementById("toBePayed")
const workButton = document.getElementById("work")
const bankButton = document.getElementById("bank")
const loanButton = document.getElementById("loanButton")
const repayButton = document.getElementById("repay")
const komputersElement = document.getElementById("laptops")
const balanceString = document.getElementById("balanceText")
const loanString = document.getElementById("loanText")
const featureString = document.getElementById("featuresText")
const laptopName = document.getElementById("laptopName")
const laptopDescription = document.getElementById("laptopDescription")
const laptopPrice = document.getElementById("laptopPrice")
const laptopImage = document.getElementById("laptopImg")
const buyLaptopButton = document.getElementById("buyNow")

// Fetches data from API.
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => komputers = data)
    .then(komputers => addKomputersToShop(komputers))

// Adds data to the website.
const addKomputersToShop = (komputers) => {
    komputers.forEach(x => addKomputerToShop(x))
    selectedLaptop = komputers[0]
    featureString.innerText = ""
    repayButton.style.display = "none"
    komputers[0].specs.forEach(x => featureString.innerText +=  x + '\n')
    laptopName.innerText = komputers[0].title
    laptopDescription.innerText = komputers[0].description
    laptopPrice.innerText = komputers[0].price + " NOK"
    laptopImage.src = "https://noroff-komputer-store-api.herokuapp.com/" + komputers[0].image
}

// Adds each data to website.
const addKomputerToShop = (komputer) => {
    const komputerElement = document.createElement("option")
    komputerElement.value = komputer.id
    komputerElement.appendChild(document.createTextNode(komputer.title))
    komputersElement.appendChild(komputerElement)
}

// Work button
const workFunction = () => {
    moneyWorkedFor += 100
    toBePayed.innerText = "Pay " + moneyWorkedFor + " Kr"
}

// Transfer work money to bank balance.
const bankFunction = () => {
    if(loanedMoney > 0) {
        if(loanedMoney > moneyWorkedFor * 0.1)  {
            loanedMoney -= moneyWorkedFor * 0.1
            moneyWorkedFor *= 0.9
            loanString.innerText = "Loan " + loanedMoney + " Kr."
        }
        else{
            moneyWorkedFor -= loanedMoney
            loanedMoney = 0
            loanString.innerText = ""
        }
    }
    totalBalance += moneyWorkedFor
    moneyWorkedFor = 0
    toBePayed.innerText = "Pay " + moneyWorkedFor + " Kr"
    balanceString.innerText = "Balance " + totalBalance + " Kr."
}

// Loan button.
const loanFunction = () => {
    if(loanedMoney > 0){
        alert("You need to pay down your loan first!")
        return
    }
    let loanAmount = prompt("How much do you want to loan?")
    if(loanAmount > totalBalance * 2){
        alert("You cant loan more than twice your current balance")
        return
    }
    repayButton.style.display = "block"
    loanedMoney += parseInt(loanAmount)
    totalBalance += loanedMoney
    balanceString.innerText = "Balance " + totalBalance + " Kr."
    loanString.innerText = "Loan " + loanedMoney + " Kr."
}

// Repays loan with work money.
const repayLoanFunction = () => {
    if(loanedMoney > moneyWorkedFor){
        loanedMoney -= moneyWorkedFor
        moneyWorkedFor = 0
        loanString.innerText = "Loan " + loanedMoney + " Kr."
        toBePayed.innerText = "Pay " + moneyWorkedFor + " Kr."
    }
    else{
        moneyWorkedFor -= loanedMoney
        loanedMoney = 0
        loanString.innerText = ""
        toBePayed.innerText = "Pay " + moneyWorkedFor + " Kr."
        repayButton.style.display = "none"
    }

}

// Handles changes when user selects new laptop.
const handleLaptopMenuChange = e => {
    selectedLaptop = komputers[e.target.selectedIndex]
    featureString.innerText = ""
    selectedLaptop.specs.forEach(x => featureString.innerText += x + "\n")

    laptopName.innerText = selectedLaptop.title
    laptopDescription.innerText = selectedLaptop.description
    laptopPrice.innerText = selectedLaptop.price + " NOK"
    laptopImage.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image
    //featureString.innerText = selectedLaptop.specs[0]
}

// Buy laptop
const buyLaptop = () => {
    if(totalBalance > selectedLaptop.price){
        totalBalance -= selectedLaptop.price
        balanceString.innerText = "Balance " + totalBalance + " Kr."
        alert("You are now the owner of the " + selectedLaptop.title + "!")
    }
    else{
        alert("You cant afford the laptop")
    }
}

// If the image link is broken then put placeholder image.
function imgError(image) {
    image.onerror = "";
    image.src = "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg";
    return true;
}

komputersElement.addEventListener("change", handleLaptopMenuChange)
workButton.addEventListener("click", workFunction)
bankButton.addEventListener("click", bankFunction)
loanButton.addEventListener("click", loanFunction)
repayButton.addEventListener("click", repayLoanFunction)
buyLaptopButton.addEventListener("click", buyLaptop)