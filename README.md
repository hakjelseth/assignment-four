# Assignment Four

This is the solution to assignment four. 
The solution includes a html file, a css stylesheet and a javascript file. <br><br>
You can find a working demo [here.](https://hakjelseth.gitlab.io/assignment-four/)
## Requirements
* A web browser
  
## Participants
This assignment was completed by **Håkon Kjelseth**.
